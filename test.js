import http from "k6/http";
import {check} from "k6";
import {Rate} from "k6/metrics";

var failedRate = new Rate("failures");
const url = "http://3.238.164.180:4000/convert?quality=50";
const gif = open("./smaller.gif");

export default function() {
  const data = {
    "ff": http.file(gif, "image/gif"),
  }; 

  let result = http.post(url, data);
  console.log(result.status); 
  check(result, {
    "is OK ": (res) => res.status === 200
  })
  failedRate.add(result.status != 200);
};